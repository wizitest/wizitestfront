import { Component, OnInit } from "@angular/core";
import { CartService } from "../services/cart.service";

@Component({
    selector: "app-order-processed-page",
    templateUrl: "./order-processed-page.component.html",
    styleUrls: ["./order-processed-page.component.scss"],
})
export class OrderProcessedPageComponent implements OnInit {
    constructor(private cartService: CartService) {}

    ngOnInit(): void {
      this.cartService.emptyCart();
    }
}
