import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { chain } from "lodash";
import { Observable, Subject } from "rxjs";

import { config } from "../../../config/config";
import { LocalStorageProducts } from "../cart-page/cart-page.component";
import { Cart } from "../types/Cart";
import { Product } from "../types/Product";

@Injectable({
    providedIn: "root",
})
export class CartService {
    private apiUrl = config.serverUrl + "/cart";

    private cartUpdatedSource = new Subject<void>();
    cartUpdated$ = this.cartUpdatedSource.asObservable();

    constructor(private http: HttpClient) {}

    getCart(startPayment: boolean = false): Observable<Cart> {
        const rawCartProductsById = localStorage.getItem("cart.products");
        let cartProductsById: LocalStorageProducts = {};
        if (rawCartProductsById !== null) {
            cartProductsById = JSON.parse(rawCartProductsById);
        }
        return this.http.post<Cart>(this.apiUrl, cartProductsById, {
            params: {
                startPayment: startPayment ? "true" : "false",
            },
        });
    }

    getCartSize(): number {
        const rawCartProductsById = localStorage.getItem("cart.products");
        let cartProductsById: LocalStorageProducts = {};
        if (rawCartProductsById !== null) {
            cartProductsById = JSON.parse(rawCartProductsById);
        }
        return chain(cartProductsById)
            .reduce((sum, nbCartProducts) => sum + nbCartProducts, 0)
            .value();
    }

    addToCart(product: Product): void {
        const rawCartProductsById = localStorage.getItem("cart.products");
        let cartProductsById: LocalStorageProducts = {};
        if (rawCartProductsById !== null) {
            cartProductsById = JSON.parse(rawCartProductsById);
        }

        if (cartProductsById[product.id] === undefined) {
            cartProductsById[product.id] = 0;
        }
        cartProductsById[product.id]++;

        localStorage.setItem("cart.products", JSON.stringify(cartProductsById));
        this.cartUpdatedSource.next();
    }

    updateCartProduct(product: Product, quantity: number): void {
        const rawCartProductsById = localStorage.getItem("cart.products");
        let cartProductsById: LocalStorageProducts = {};
        if (rawCartProductsById !== null) {
            cartProductsById = JSON.parse(rawCartProductsById);
        }

        if (quantity === 0) {
            delete cartProductsById[product.id];
        } else {
            cartProductsById[product.id] = quantity;
        }

        localStorage.setItem("cart.products", JSON.stringify(cartProductsById));
        this.cartUpdatedSource.next();
    }

    emptyCart(): void {
        localStorage.setItem("cart.products", JSON.stringify({}));
        this.cartUpdatedSource.next();
    }

    getProductQuantity(productId: number): number {
        const rawCartProductsById = localStorage.getItem("cart.products");
        let cartProductsById: LocalStorageProducts = {};
        if (rawCartProductsById !== null) {
            cartProductsById = JSON.parse(rawCartProductsById);
        }

        return cartProductsById[productId];
    }
}
