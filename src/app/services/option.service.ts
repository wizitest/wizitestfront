import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ProductOption, ProductOptions } from "../types/ProductOption";
import { Observable } from "rxjs";

import { config } from "../../../config/config";

@Injectable({
    providedIn: "root",
})
export class OptionService {
    private apiUrl = config.serverUrl + "/option";

    constructor(private http: HttpClient) {}

    getOptions(): Observable<ProductOptions> {
        return this.http.get<ProductOptions>(`${this.apiUrl}`);
    }
}
