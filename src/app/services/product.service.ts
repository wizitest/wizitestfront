import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { Product } from "../types/Product";
import { RefreshProductsEventParams } from "../product-grid/product-grid.component";
import { GetProductResponse as GetProductsResponse } from "../types/GetProductResponse";
import { config } from "../../../config/config";

@Injectable({
    providedIn: "root",
})
export class ProductService {
    private apiUrl = config.serverUrl + "/product";

    constructor(private http: HttpClient) {}

    getProducts(
        params?: RefreshProductsEventParams,
        categoryId?: number
    ): Observable<GetProductsResponse> {
        let payload = new HttpParams();
        payload = payload.append(
            "sortBy",
            params?.sortOrder ? params.sortOrder : "ASC"
        );

        if (categoryId !== undefined) {
            payload = payload.append("categoryId", categoryId.toString());
        }

        if (params) {
            if (params.filters) {
                params.filters.forEach((optionId: number) => {
                    payload = payload.append("options[]", optionId.toString());
                });
            }
            if (params.itemsPerPage !== undefined && params.page !== undefined) {
                payload = payload.append("page", params.page.toString());
                payload = payload.append("itemsPerPage", params.itemsPerPage.toString());
            }
        }

        return this.http.get<GetProductsResponse>(this.apiUrl, {
            params: payload,
        });
    }

    getProductById(id: number): Observable<Product> {
        return this.http.get<Product>(`${this.apiUrl}/${id}`);
    }
}
