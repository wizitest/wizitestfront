import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Category } from "../types/Category";
import { config } from "../../../config/config";

@Injectable({
    providedIn: "root",
})
export class CategoryService {
    private apiUrl = config.serverUrl + "/category";

    constructor(private http: HttpClient) {}

    getCategories(): Observable<Category[]> {
        return this.http.get<Category[]>(this.apiUrl);
    }

    getCategoryById(categoryId: number): Observable<Category>  {
        return this.http.get<Category>(`${this.apiUrl}/${categoryId}`);
    }
}
