import {
    AfterViewInit,
    Component,
    ElementRef,
    Input,
    OnInit,
    ViewChild,
} from "@angular/core";

import { config } from "../../../config/config";
import { CartService } from "../services/cart.service";
import { Cart } from "../types/Cart";

declare var Stripe: any;

@Component({
    selector: "app-payment-page",
    templateUrl: "./payment-page.component.html",
    styleUrls: ["./payment-page.component.scss"],
})
export class PaymentPageComponent implements OnInit, AfterViewInit {
    @Input() next!: () => void;

    cart?: Cart;

    stripe!: any;
    card!: any;
    cardErrors!: any;

    isProcessingPayment = false;

    @ViewChild("cardElement") cardElement!: ElementRef;

    constructor(private cartService: CartService) {}

    ngOnInit(): void {
        this.stripe = Stripe(config.stripePublicKey);
        this.cartService.getCart(true).subscribe((cart) => (this.cart = cart));
    }

    ngAfterViewInit(): void {
        const elements = this.stripe.elements();

        this.card = elements.create("card");
        this.card.mount(this.cardElement.nativeElement);

        this.card.addEventListener("change", ({ error }: any) => {
            this.cardErrors = error && error.message;
        });
        this.card.addEventListener("change", ({ error }: any) => {
            this.cardErrors = error && error.message;
        });
    }

    async handleForm(event: any): Promise<void> {
        event.preventDefault();
        if (this.cart && this.cart.stripePaymentIntentSecret) {
            this.isProcessingPayment = true;
            const result = await this.stripe.confirmCardPayment(
                this.cart.stripePaymentIntentSecret,
                {
                    payment_method: {
                        card: this.card,
                        billing_details: {
                            name: "Sarah TEST",
                        },
                    },
                }
            );
            this.isProcessingPayment = false;
            if (result.error) {
                console.error(result.error.message);
                this.cardErrors = result.error.message;
            } else {
                if (result.paymentIntent.status === "succeeded") {
                    this.next();
                }
            }
        }
    }
}
