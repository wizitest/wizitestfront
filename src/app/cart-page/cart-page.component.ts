import { Component, Input, OnInit } from "@angular/core";
import { config } from "../../../config/config";
import { CartService } from "../services/cart.service";
import { Cart } from "../types/Cart";
import { Product } from "../types/Product";

export type LocalStorageProducts = { [productId: number]: number };

@Component({
    selector: "app-cart-page",
    templateUrl: "./cart-page.component.html",
    styleUrls: ["./cart-page.component.scss"],
})
export class CartPageComponent implements OnInit {
    cart: Cart | undefined;
    defaultImage: string = config.defaultImage;

    @Input() next!: () => void;

    constructor(private cartService: CartService) {}

    ngOnInit(): void {
        this.getProducts();
    }

    getProducts(): void {
        this.cartService.getCart().subscribe((cart: Cart) => {
            this.cart = cart;
        });
    }
}
