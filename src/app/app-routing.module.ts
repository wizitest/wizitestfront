import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CartPageComponent } from "./cart-page/cart-page.component";
import { CategoryPageComponent } from "./category-page/category-page.component";
import { MainPageComponent } from "./main-page/main-page.component";
import { OrderProcessComponent } from "./order-process/order-process.component";
import { ProductPageComponent } from "./product-page/product-page.component";

const routes: Routes = [
    { path: "", component: MainPageComponent },
    { path: "product/:id", component: ProductPageComponent },
    { path: "category/:id", component: CategoryPageComponent },
    { path: "cart", component: OrderProcessComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: "reload" })],
    exports: [RouterModule],
})
export class AppRoutingModule {}
