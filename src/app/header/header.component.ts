import { Component, OnInit } from "@angular/core";

import { CartService } from "../services/cart.service";
import { CategoryService } from "../services/category.service";
import { Category } from "../types/Category";

@Component({
    selector: "app-header",
    templateUrl: "./header.component.html",
    styleUrls: ["./header.component.scss"],
})
export class HeaderComponent implements OnInit {
    categories: Category[] = [];
    cartNbProducts = 0;

    constructor(
        private categoryService: CategoryService,
        private cartService: CartService
    ) {}

    ngOnInit(): void {
        this.getCategories();
        this.getCart();

        this.cartService.cartUpdated$.subscribe(() => this.getCart());
    }

    getCategories(): void {
        this.categoryService
            .getCategories()
            .subscribe((categories) => (this.categories = categories));
    }

    getCart(): void {
        this.cartNbProducts = this.cartService.getCartSize();
    }
}
