import { Component, OnInit, ViewChild } from "@angular/core";
import { MatHorizontalStepper } from "@angular/material/stepper";
import { NavigationEnd, Router } from "@angular/router";

@Component({
    selector: "app-order-process",
    templateUrl: "./order-process.component.html",
    styleUrls: ["./order-process.component.scss"],
})
export class OrderProcessComponent implements OnInit {
    @ViewChild("stepper") stepper!: MatHorizontalStepper;

    currentStep = 0;
    canGoBack = true;

    constructor(private router: Router) {}
    ngOnInit(): void {
        this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                // User has clicked the cart icon, we go back to the cart view
                this.canGoBack = true;
                this.goToFirstStep();
            }
        });
    }

    goToNextStep(): void {
        if (this.stepper) {
            this.stepper.selected.completed = true;
            this.stepper.next();
            this.currentStep = this.stepper.selectedIndex;
            if (this.stepper.steps.length - 1 === this.currentStep) {
                this.canGoBack = false;
            }
        }
    }

    goToFirstStep(): void {
        if (this.stepper) {
            this.currentStep = 0;
            this.stepper.reset();
        }
    }
}
