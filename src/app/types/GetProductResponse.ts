import { Product } from "./Product";

export interface GetProductResponse {
    products: Product[];
    pagination: {
        page: number;
        itemsPerPage: number;
        totalNb: number;
    };
}
