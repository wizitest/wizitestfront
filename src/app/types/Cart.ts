import { Product } from "./Product";

export interface Cart {
    products: Product[];
    totalPriceTaxExcluded: number;
    totalPriceTaxIncluded: number;
    stripePaymentIntentSecret?: string;
}
