export type ProductOptions = { [category: string]: ProductOption[] };

export interface ProductOption {
    id: number;
    label: string;
    category: string;
    order: number;
}
