import { Image } from "./Image";
import { ProductOption } from "./ProductOption";

export interface Product {
    id: number;
    categoryId: number;
    name: string;
    shortDescription: string;
    description: string;
    priceTaxExcluded: number;
    priceTaxIncluded?: number;
    options: ProductOption[];
    images: Image[];
    quantity?: number;
}
