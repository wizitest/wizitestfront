export interface Image {
    id: number;
    filename: string;
    path: string;
}
