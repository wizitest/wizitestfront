import { Component, Input, OnInit, EventEmitter } from "@angular/core";
import { PageEvent } from "@angular/material/paginator";
import { chain } from "lodash";

import { config } from "../../../config/config";
import { CartService } from "../services/cart.service";
import { OptionService } from "../services/option.service";
import { ProductService } from "../services/product.service";
import { Category } from "../types/Category";
import { Product } from "../types/Product";
import { ProductOption } from "../types/ProductOption";

export interface Filter {
    label: string;
    options: ProductOption[];
}

export interface RefreshProductsEventParams {
    sortOrder?: string;
    filters?: number[];
    page?: number;
    itemsPerPage?: number;
}

@Component({
    selector: "app-product-grid",
    templateUrl: "./product-grid.component.html",
    styleUrls: ["./product-grid.component.scss"],
})
export class ProductGridComponent implements OnInit {
    @Input() category?: Category;
    @Input() needToRefreshEvent!: EventEmitter<Category>;

    products: Product[] = [];
    isLoading = true;

    defaultImage: string = config.defaultImage;

    // Sort
    sortByOptions = [
        { label: "Trier par prix croissant", value: "ASC" },
        { label: "Trier par prix décroissant", value: "DESC" },
    ];
    selectedSortOption = this.sortByOptions[0];

    // Filters
    filters: Filter[] = [];
    selectedFilters: number[] = [];

    // Pagination
    page = 0;
    itemsPerPage = 8;
    totalNb = 0;

    constructor(
        private productService: ProductService,
        private cartService: CartService,
        private optionService: OptionService
    ) {}

    ngOnInit(): void {
        if (this.needToRefreshEvent) {
            this.needToRefreshEvent.subscribe((category: Category) => {
                this.category = category;
                this.page = 0;
                this.getProducts();
                this.getFilters();
            });
        } else {
            this.page = 0;
            this.getProducts();
            this.getFilters();
        }
    }

    getProducts(params?: RefreshProductsEventParams): void {
        if (params === undefined) {
            params = {};
        }
        params.page = this.page;
        params.itemsPerPage = this.itemsPerPage;
        this.isLoading = true;
        this.productService
            .getProducts(params, this.category?.id)
            .subscribe((getProductResponse) => {
                this.totalNb = getProductResponse.pagination.totalNb;
                this.products = getProductResponse.products;
                this.isLoading = false;
                this.products.forEach((product) => {
                    product.quantity = this.cartService.getProductQuantity(
                        product.id
                    );
                });
            });
    }

    getFilters(): void {
        this.optionService.getOptions().subscribe((optionsByCategory) => {
            this.filters = chain(optionsByCategory)
                .map((options, category) => ({ label: category, options }))
                .value();
        });
    }

    selectFilter(option: ProductOption, checked: boolean): void {
        if (checked) {
            this.selectedFilters.push(option.id);
        } else {
            this.selectedFilters = this.selectedFilters.filter(
                (id) => id !== option.id
            );
        }
        this.getProducts({
            sortOrder: this.selectedSortOption.value,
            filters: this.selectedFilters,
        });
    }

    addToCart(product: Product): void {
        if (product) {
            this.cartService.addToCart(product);
            product.quantity = this.cartService.getProductQuantity(product.id);
        }
    }

    changeSortOrder(): void {
        this.getProducts({
            sortOrder: this.selectedSortOption.value,
            filters: this.selectedFilters,
        });
    }

    changePage(event: PageEvent): void {
        this.page = event.pageIndex;
        this.getProducts();
    }
}
