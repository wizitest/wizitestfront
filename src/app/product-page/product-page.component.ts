import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { ProductService } from "../services/product.service";
import { Product } from "../types/Product";
import { Image } from "../types/Image";
import { CartService } from "../services/cart.service";

@Component({
    selector: "app-product-page",
    templateUrl: "./product-page.component.html",
    styleUrls: ["./product-page.component.scss"],
})
export class ProductPageComponent implements OnInit {
    product: Product | null = null;
    currentImage: Image | null = null;

    isLoading = true;

    constructor(
        private route: ActivatedRoute,
        private productService: ProductService,
        private cartService: CartService
    ) {}

    ngOnInit(): void {
        this.getProduct();
    }

    getProduct(): void {
        const id = this.route.snapshot.paramMap.get("id");
        if (id === null) {
            // redirect to 404
            return;
        }
        this.isLoading = true;
        this.productService.getProductById(+id).subscribe((product) => {
            this.product = product;
            if (this.product.images.length > 0) {
                this.currentImage = this.product.images[0];
                this.isLoading = false;
            }
            this.product.quantity = this.cartService.getProductQuantity(
                this.product.id
            );
        });
    }

    addToCart(): void {
        if (this.product) {
            this.cartService.addToCart(this.product);
            this.product.quantity = this.cartService.getProductQuantity(
                this.product.id
            );
        }
    }

    changeCurrentImage(image: Image): void {
        this.currentImage = image;
    }
}
