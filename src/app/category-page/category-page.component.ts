import { Component, OnInit, EventEmitter } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { CategoryService } from "../services/category.service";
import { Category } from "../types/Category";

@Component({
    selector: "app-category-page",
    templateUrl: "./category-page.component.html",
    styleUrls: ["./category-page.component.scss"],
})
export class CategoryPageComponent implements OnInit {
    category: Category | undefined;

    categoryChangedEvent = new EventEmitter<Category>();

    constructor(
        private route: ActivatedRoute,
        private categoryService: CategoryService
    ) {}

    ngOnInit(): void {
        this.route.params.subscribe((routeParams) => {
            this.getCategory();
        });
    }

    getCategory(): void {
        const categoryId = this.route.snapshot.paramMap.get("id");
        if (categoryId === null) {
            // redirect to 404
            return;
        }

        this.categoryService
            .getCategoryById(+categoryId)
            .subscribe((category) => {
                this.category = category;
                this.categoryChangedEvent.emit(category);
            });
    }
}
