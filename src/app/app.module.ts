import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";

import { MainPageComponent } from "./main-page/main-page.component";
import { ProductPageComponent } from "./product-page/product-page.component";
import { HeaderComponent } from "./header/header.component";
import { CartPageComponent } from "./cart-page/cart-page.component";
import { CategoryPageComponent } from "./category-page/category-page.component";
import { ProductGridComponent } from "./product-grid/product-grid.component";
import { PriceComponent } from "./price/price.component";
import { OrderProcessComponent } from "./order-process/order-process.component";
import { PaymentPageComponent } from "./payment-page/payment-page.component";
import { OrderProcessedPageComponent } from "./order-processed-page/order-processed-page.component";
import { LoaderComponent } from "./loader/loader.component";
import { QuantitySelectorComponent } from "./quantity-selector/quantity-selector.component";

import { MatGridListModule } from "@angular/material/grid-list";
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { MatDividerModule } from "@angular/material/divider";
import { MatSelectModule } from "@angular/material/select";
import { MatBadgeModule } from "@angular/material/badge";
import { MatInputModule } from "@angular/material/input";
import { MatStepperModule } from "@angular/material/stepper";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatRippleModule } from "@angular/material/core";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";

@NgModule({
    declarations: [
        AppComponent,
        MainPageComponent,
        ProductPageComponent,
        HeaderComponent,
        CartPageComponent,
        CategoryPageComponent,
        ProductGridComponent,
        PriceComponent,
        OrderProcessComponent,
        PaymentPageComponent,
        OrderProcessedPageComponent,
        LoaderComponent,
        QuantitySelectorComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule,
        MatGridListModule,
        MatIconModule,
        MatButtonModule,
        MatCardModule,
        MatDividerModule,
        MatSelectModule,
        MatBadgeModule,
        MatInputModule,
        MatStepperModule,
        MatCheckboxModule,
        MatPaginatorModule,
        MatRippleModule,
        MatProgressSpinnerModule,
    ],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {}
