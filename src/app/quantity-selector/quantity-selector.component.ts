import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { CartService } from "../services/cart.service";
import { Product } from "../types/Product";

@Component({
    selector: "app-quantity-selector",
    templateUrl: "./quantity-selector.component.html",
    styleUrls: ["./quantity-selector.component.scss"],
})
export class QuantitySelectorComponent implements OnInit {
    @Input() product!: Product;
    @Output() productQuantityChanged = new EventEmitter<void>();

    constructor(private cartService: CartService) {}

    ngOnInit(): void {}

    updateQuantity(product: Product, modifier?: number): void {
        if (product && product.quantity !== undefined) {
            if (modifier !== undefined) {
                product.quantity += modifier;
            }
            this.cartService.updateCartProduct(product, product.quantity);
            this.productQuantityChanged.emit();
        }
    }
}
