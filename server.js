const express = require("express");

const app = express();

app.use(express.static("./dist/wizitest-front"));
app.get("/*", function (req, res) {
    res.sendFile("index.html", { root: "dist/wizitest-front/" });
});
app.listen(process.env.PORT || 8080);
