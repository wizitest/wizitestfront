## Installation

```
yarn install
// Ou
npm install
```

## Choix du serveur

Dans config/config.ts :

```typescript
// Pour tester avec le serveur hébergé sur Heroku
serverUrl: "https://wizitest-server.herokuapp.com";

// Pour tester avec le serveur en local
serverUrl: "http://localhost:8000";
```

## Lancement

```
ng serve
```

Si l'onglet ne s'ouvre pas automatiquement, rendez-vous sur http://localhost:4200/
